// stage 1

import TestRepository, {makeTestRepository} from './TestRepository'

export default class DependentService {
  getInnerData(): string {
    return TestRepository.getData();
    // return makeTestRepository().getData();
  }
}


// stage 2


// import {IRepository} from "./TestRepository";
//
// export default class DependentService {
//
//   testRepository: IRepository;
//
//   constructor({testRepository}) {
//     this.testRepository = testRepository;
//   }
//
//   getInnerData(): string {
//     return this.testRepository.getData();
//   }
// }
