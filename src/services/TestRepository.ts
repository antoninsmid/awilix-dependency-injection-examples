// stage 1
export default class TestRepository {

  private static cnt = 0;
  private static data = 'Hello World: ';

  public static getData(): string {
    this.cnt ++;
    return this.data + this.cnt;
  }
}

export const makeTestRepository = () => {
  const data = 'Hello World: ';
  let cnt = 0;

  const getData = () => {
    cnt++;
    return data + cnt;
  }

  return {
    getData
  }
}



// stage 2, 3

// export interface IRepository {
//   getData(): string;
// }
//
// export default class TestRepository implements IRepository {
//   private cnt = 0;
//   private data = 'Hello World: ';
//
//   public getData(): string {
//     this.cnt ++;
//     return this.data + this.cnt;
//   }
// }
//
// export const makeTestRepository:(() => IRepository) = () => {
//   const data = 'Hello World: ';
//   let cnt = 0;
//
//   const getData = () => {
//     cnt++;
//     return data + cnt;
//   }
//
//   return {
//     getData
//   }
// }
