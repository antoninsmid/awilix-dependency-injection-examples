// stage 1 - directly inject module; static dependencies

import DependentService from "./services/DependentService";

const dependentService = new DependentService();
console.log("1: ", dependentService.getInnerData());
console.log("2: ",dependentService.getInnerData());



// stage 2

// import DependentService from "./services/DependentService";
// import TestRepository, {IRepository} from "./services/TestRepository";
//
// // prepare
// const testRepository: IRepository = new TestRepository();
// const dependentService = new DependentService({testRepository});
//
// // use
// console.log("1: ", dependentService.getInnerData());
// console.log("2: ",dependentService.getInnerData());



// stage 3 - introduce DI container, test with class repo and function repo

// import DependentService from "./services/DependentService";
// import TestRepository, {IRepository, makeTestRepository} from "./services/TestRepository";
// import {asClass, asFunction, AwilixContainer, createContainer} from "awilix";
//
//
// interface ICradle {
//   testService: DependentService,
//   testRepository: IRepository,
//   testFnRepository: IRepository
// }
//
// // Create the container
// const container = createContainer<ICradle>();
//
// // Register the classes
// container.register({
//   testService: asClass(DependentService),
//   testRepository: asClass(TestRepository),
//   testFnRepository: asFunction(makeTestRepository)
// })

// const dependentService1: DependentService = container.cradle.testService;
// const dependentService2: DependentService = container.resolve<DependentService>('testService');
//
// console.log("1: ", dependentService1.getInnerData());
// console.log("2: ", dependentService2.getInnerData());



// stage 4 - add scopes and show lifetime //{lifetime: "SINGLETON"}

// class InnerServiceUser {
//     private service: DependentService;
//     constructor(container: AwilixContainer) {
//         this.service = container.resolve<DependentService>('testService');
//     }
//     public useService(){
//         console.log('InnerServiceUser: ' + this.service.getInnerData());
//     }
// }
//
// class ServiceUser {
//
//     private scope: AwilixContainer;
//     private service: DependentService;
//     private innerServiceUser: InnerServiceUser;
//
//     constructor() {
//         this.scope = container.createScope<ICradle>();
//         this.service = this.scope.resolve<DependentService>('testService');
//         this.innerServiceUser = new InnerServiceUser(this.scope);
//     }
//
//     public useService(){
//         this.innerServiceUser.useService();
//         console.log('ServiceUser: ' + this.service.getInnerData());
//     }
// }
//
// const serviceUser1 = new ServiceUser();
// const serviceUser2 = new ServiceUser();
//
// serviceUser1.useService();
// serviceUser2.useService();



