### Simple Axilix showcase with typescript

- Dependency injection
  - technique to help creating independent modules
  - Mainly for node.js; but useful for FE only services as well
  - typical for Java, C#...
- DI vs Require
  - require - **coupled with a specific dependency**
  - how to test? need to stub it with Sinon/Jest.mock
  - better to pass the dependencies as parameters
  - -> freedom to change implementation at any point
- then we would need to prepare (spawn) all stuff in advance
- DI Awilix resolves the dependencies in a container for us
- With Vue - provide to all components, then @Inject
  - https://v3.vuejs.org/guide/component-provide-inject.html#working-with-reactivity
  - show in Hume
